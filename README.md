# AzDetectSuite

A collection of ARM-based detections for Azure/AzureAD based TTPs

## Execution

| ID          | Name                                                             |Requires Azure Monitor Agent?|Deploy|
| ----------- |------------------------------------------------------------------|-----|------|
| AZT301.1    | Virtual Machine Scripting: RunCommand|N|[![Deploy to Azure](https://aka.ms/deploytoazurebutton)](https://portal.azure.com/#create/Microsoft.Template/uri/https%3A%2F%2Fgitlab.com%2Fads7810318%2FADS6208%2F-%2Fraw%2Fmain%2FExecution%2FAZT301%2FAZT301-1.json)|
| AZT301.1    | Virtual Machine Scripting: RunCommand with PowerShell Logging |Y|[![Deploy to Azure](https://aka.ms/deploytoazurebutton)](https://portal.azure.com/#create/Microsoft.Template/uri/https://gitlab.com/ads7810318/ADS6208/-/raw/main/Execution/AZT301/AZT301-1-AMA.json)|
| AZT301.2    | Virtual Machine Scripting: CustomScriptExtension|N|[![Deploy to Azure](https://aka.ms/deploytoazurebutton)](https://portal.azure.com/#create/Microsoft.Template/uri/https://gitlab.com/ads7810318/ADS6208/-/raw/main/Execution/AZT301/AZT301-2.json)|
| AZT301.2    | Virtual Machine Scripting: CustomScriptExtension with PowerShell Logging|Y|[![Deploy to Azure](https://aka.ms/deploytoazurebutton)](https://portal.azure.com/#create/Microsoft.Template/uri/https://gitlab.com/ads7810318/ADS6208/-/raw/main/Execution/AZT301/AZT301-2-AMA.json)|


### Contributing

This project welcomes contributions and suggestions.  Most contributions require you to agree to a
Contributor License Agreement (CLA) declaring that you have the right to, and actually do, grant us
the rights to use your contribution. For details, visit https://cla.opensource.microsoft.com.

When you submit a pull request, a CLA bot will automatically determine whether you need to provide
a CLA and decorate the PR appropriately (e.g., status check, comment). Simply follow the instructions
provided by the bot. You will only need to do this once across all repos using our CLA.

This project has adopted the [Microsoft Open Source Code of Conduct](https://opensource.microsoft.com/codeofconduct/).
For more information see the [Code of Conduct FAQ](https://opensource.microsoft.com/codeofconduct/faq/) or
contact [opencode@microsoft.com](mailto:opencode@microsoft.com) with any additional questions or comments.

### Trademarks

This project may contain trademarks or logos for projects, products, or services. Authorized use of Microsoft 
trademarks or logos is subject to and must follow 
[Microsoft's Trademark & Brand Guidelines](https://www.microsoft.com/en-us/legal/intellectualproperty/trademarks/usage/general).
Use of Microsoft trademarks or logos in modified versions of this project must not cause confusion or imply Microsoft sponsorship.
Any use of third-party trademarks or logos are subject to those third-party's policies.